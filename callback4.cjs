const fs = require('fs');
const boardIdInformation = require('./callback1.cjs');
const listsOfBoard = require('./callback2.cjs');
const cardDetails = require('./callback3.cjs');

function thanosDetails(boardsPath, listsPath, cardsPath, thanosId) {
    setTimeout(() => {
        boardIdInformation(boardsPath, thanosId, (err, data) => {
            if (err) {
                console.error(err);
            } else {
                console.log("Boards data read sucessfully");
                console.log(data);
                listsOfBoard(listsPath, thanosId, (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log('Lists data read sucessfully');
                        console.log(data);
                        const mindId = data.find((id) => {
                            return id.name == 'Mind';
                        });
                        cardDetails(cardsPath, mindId.id, (err, data) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log('Cards data read sucessfully');
                                console.log((data));
                            }
                        });
                    }
                });
            }
        });

    }, 2 * 1000);
}


module.exports = thanosDetails;