const path = require('path');
const thanosDetailsWithAllCards = require('../callback6.cjs');

const boardsPath = path.join(__dirname, '../boards.json');
const listsPath = path.join(__dirname, '../lists.json');
const cardsPath = path.join(__dirname, '../cards.json');

thanosDetailsWithAllCards(boardsPath, listsPath, cardsPath, 'mcu453ed');