const path = require('path');
const thanosDetails = require('../callback4.cjs');

const boardsPath = path.join(__dirname, '../boards.json');
const listsPath = path.join(__dirname, '../lists.json');
const cardsPath = path.join(__dirname, '../cards.json');

thanosDetails(boardsPath, listsPath, cardsPath, 'mcu453ed');