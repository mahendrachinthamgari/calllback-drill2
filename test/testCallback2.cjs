const path = require('path');
const listsOfBoard = require('../callback2.cjs');

const filePath = path.join(__dirname, '../lists.json');

const callback = (err, data) => {
    if (err) {
        console.error(err);
    } else {
        console.log("Data read sucessfully");
        console.log(data);
    }
}

listsOfBoard(filePath, 'mcu453ed', callback);