const path = require('path');
const thanosDetailsWithMindAndSpace = require('../callback5.cjs');

const boardsPath = path.join(__dirname, '../boards.json');
const listsPath = path.join(__dirname, '../lists.json');
const cardsPath = path.join(__dirname, '../cards.json');

thanosDetailsWithMindAndSpace(boardsPath, listsPath, cardsPath, 'mcu453ed');