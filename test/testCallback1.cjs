const path = require('path');
const boardIdInformation = require('../callback1.cjs');

const filePath = path.join(__dirname, '../boards.json');

const callback = (err, data) => {
    if (err) {
        console.error(err);
    } else {
        console.log("Data read sucessfully");
        console.log(data);
    }
}

boardIdInformation(filePath, 'mcu453ed', callback);