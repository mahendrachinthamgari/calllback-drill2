const fs = require('fs');

function cardDetails(filePath, listId, callback) {
    setTimeout(() => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                callback(err);
            } else {
                let idDetails = JSON.parse(data);
                callback(null, idDetails[listId]);
            }
        });
    }, 2 * 1000);
}


module.exports = cardDetails;