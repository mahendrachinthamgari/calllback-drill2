const fs = require('fs');

function boardIdInformation(filePath, boardId, callback) {
    setTimeout(() => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                callback(err);
            } else {
                let information = JSON.parse(data).filter((requireData) => {
                    return requireData.id == boardId;
                });
                callback(null, information);
            }
        });
    }, 2 * 1000);
}


module.exports = boardIdInformation;