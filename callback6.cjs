const fs = require('fs');
const boardIdInformation = require('./callback1.cjs');
const listsOfBoard = require('./callback2.cjs');
const cardDetails = require('./callback3.cjs');

function thanosDetailsWithAllCards(boardsPath, listsPath, cardsPath, thanosId) {
    setTimeout(() => {
        boardIdInformation(boardsPath, thanosId, (err, data) => {
            if (err) {
                console.error(err);
            } else {
                console.log("Boards data read sucessfully");
                console.log(data);
                listsOfBoard(listsPath, thanosId, (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log('Lists data read sucessfully');
                        console.log(data);
                        let idList = data.map((element) => {
                            return element.id;
                        });
                        for (let index = 0; index < idList.length; index++) {
                            cardDetails(cardsPath, idList[index], (err, data) => {
                                if (err) {
                                    console.error(err);
                                } else {
                                    console.log(data);
                                }
                            });
                        }
                    }
                });
            }
        });

    }, 2 * 1000);
}


module.exports = thanosDetailsWithAllCards;