const fs = require('fs');

function listsOfBoard(filePath, boardId, callback) {
    setTimeout(() => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                callback(err);
            } else {
                dataLists = JSON.parse(data)[boardId];
                callback(null, dataLists);
            }
        })
    }, 2 * 1000);
}


module.exports = listsOfBoard;